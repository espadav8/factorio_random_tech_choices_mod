-- use-game-time-with-seed

local permission_group
local generator

local function can_research(tech)
    if (not tech.enabled) or tech.researched then
        return false
    end

    for _, prerequisite in pairs(tech.prerequisites) do
        if not prerequisite.researched then
            return false
        end
    end

    return true
end

local function pick_new_research(force)
    if force.current_research then
        return
    end

    local available_tech = {}
    for tech_name, tech in pairs(force.technologies) do
        if can_research(tech) then
            table.insert(available_tech, tech_name)
        end
    end

    local random_tech = available_tech[math.random(#available_tech)]
    force.add_research(random_tech)
end

local function assign_new_research()
    for _, force in pairs(game.forces) do
        pick_new_research(force)
    end
end

local function add_players_to_permission_group()
    if not generator then
        if settings.startup["random-tech-choices-use-random-seed"].value then
            generator = math.random
        else
            generator = game.create_random_generator()
        end
    end

    if not permission_group then
        permission_group = game.permissions.create_group('no-research-changes')
        permission_group.set_allows_action(defines.input_action.cancel_research, false)
        permission_group.set_allows_action(defines.input_action.start_research, false)
        permission_group.set_allows_action(defines.input_action.open_technology_gui, false)
    end

    for _, player in pairs(game.players) do
        permission_group.add_player(player)
    end
end

local function on_player_created(event)
    permission_group.add_player(event.player_index)
    assign_new_research()
end

script.on_init(add_players_to_permission_group)
script.on_configuration_changed(add_players_to_permission_group)
script.on_event(defines.events.on_player_created, on_player_created)
script.on_event(defines.events.on_research_finished, assign_new_research)
script.on_event(defines.events.on_research_cancelled, assign_new_research)
