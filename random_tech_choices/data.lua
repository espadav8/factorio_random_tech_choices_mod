local fix_science_packs = {
    ["logistic-science-pack"] = {
        "physical-projectile-damage-2",
        "physical-projectile-damage-3",
        "physical-projectile-damage-4",
        "physical-projectile-damage-5",
        "physical-projectile-damage-6",
        "physical-projectile-damage-7",

        "weapon-shooting-speed-2",
        "weapon-shooting-speed-3",
        "weapon-shooting-speed-4",
        "weapon-shooting-speed-5",
        "weapon-shooting-speed-6",
    },
    ["military-science-pack"] = {
        "physical-projectile-damage-3",
        "physical-projectile-damage-4",
        "physical-projectile-damage-5",
        "physical-projectile-damage-6",

        "weapon-shooting-speed-3",
        "weapon-shooting-speed-4",
        "weapon-shooting-speed-5",
        "weapon-shooting-speed-6",
    },
    ["chemical-science-pack"] = {
        "physical-projectile-damage-5",
        "physical-projectile-damage-6",
        "physical-projectile-damage-7",

        "weapon-shooting-speed-5",
        "weapon-shooting-speed-6",
    },
    ["utility-science-pack"] = {
        "physical-projectile-damage-6",
        "physical-projectile-damage-7",

        "weapon-shooting-speed-6",
    }
}

for pack, techs in pairs(fix_science_packs) do
    for _, tech in pairs(techs) do
        local x = data.raw["technology"][tech]
        table.insert(x.prerequisites, pack)
    end
end
